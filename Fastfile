# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane
fastlane_version "2.70.0"

default_platform(:ios)

platform :ios do
  before_all do
      ENV["SLACK_URL"] = "https://hooks.slack.com/services/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"
      ENV["CERT_OUTPUT_PATH"] ||= "fastlane/certs"
      ENV["SIGH_OUTPUT_PATH"] ||= "fastlane/provisionings"
      ENV["MATCH_PASSWORD"] ||= "Acciio@123"
      ENV["CRASHLYTICS_API_KEY"] ||= "d65c515062f79b7f4750c2efd2570d3cc7e3ce04"
      ENV["CRASHLYTICS_BUILD_SECRET"] ||= "7105ead3a5485b336aac7611642ca6b0c2eeb8a10bad1be7c77f6e5e14144ea1"
      ENV['CURRENT_FOLDER'] = File.expand_path(File.join(File.dirname(__FILE__), ".."))
  end

  desc "Setup"
  lane :setup do
    cocoapods
  end

  desc "Certs"
  lane :certs do
    certs_adhoc
  end

  desc "Increase build number"
  lane :commit_build_number do
    increment_build_number
    commit_version_bump(message: "[Fastlane] Increase build number")
    push_to_git_remote
  end

  desc "This will also make sure the profile is up to date"
  lane :beta do |options|
    # Ask to enter a changelog
    target_branch = options[:branch] || 'develop'
    changelog = prompt(text: "Enter the change log: ", multi_line_end_keyword: "END")
    ensure_git_status_clean
    ensure_git_branch(branch: target_branch)
    increment_build_number
    commit_version_bump(message: "[Fastlane] Increase build number")
    push_to_git_remote

    # Certificates
    certs_adhoc

    # Build and sign
    configuration = options[:config] || "Adhoc"
    gym(configuration: configuration)

    # Distribute
    groups = options[:groups] || "all"
    crashlytics(
      crashlytics_path: './Pods/Crashlytics/',
      notes: changelog,
      ipa_path: 'fastlane/build/KakaoHome.ipa',
      api_token: ENV["CRASHLYTICS_API_KEY"],
      build_secret: ENV["CRASHLYTICS_BUILD_SECRET"],
      groups: [groups],
      notifications: true
    )
  end

  private_lane :certs_adhoc do
    match(type: "adhoc", force_for_new_devices: true)
  end
end
